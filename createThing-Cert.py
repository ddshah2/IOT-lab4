################################################### Connecting to AWS
import boto3

import json
################################################### Create random name for things
import random
import string
import os

################################################### Parameters for Thing
thingArn = ''
thingId = ''
thingName = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(15)])
defaultPolicyName = 'GGTest_Group_Core-policy'
###################################################

def createThing(i):
	global thingClient
	thingResponse = thingClient.create_thing(
		thingName = thingName
	)
	data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
	for element in data: 
		if element == 'thingArn':
			thingArn = data['thingArn']
		elif element == 'thingId':
			thingId = data['thingId']
	createCertificate(i)
def createCertificate(i):
	global thingClient
	certResponse = thingClient.create_keys_and_certificate(
			setAsActive = True
	)
	data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
	for element in data: 
			if element == 'certificateArn':
					certificateArn = data['certificateArn']
			elif element == 'keyPair':
					PublicKey = data['keyPair']['PublicKey']
					PrivateKey = data['keyPair']['PrivateKey']
			elif element == 'certificatePem':
					certificatePem = data['certificatePem']
			elif element == 'certificateId':
					certificateId = data['certificateId']

	mkdir_path = f'./certificates/device_{i}'
	if not os.path.exists(mkdir_path):
			os.makedirs(mkdir_path)

	with open(f'./certificates/device_{i}/device_{i}.public.pem', 'w') as outfile:
			outfile.write(PublicKey)
	with open(f'./certificates/device_{i}/device_{i}.private.pem', 'w') as outfile:
			outfile.write(PrivateKey)
	with open(f'./certificates/device_{i}/device_{i}.certificate.pem', 'w') as outfile:
			outfile.write(certificatePem)

	response = thingClient.attach_policy(
			policyName = defaultPolicyName,
			target = certificateArn
	)
	response = thingClient.attach_thing_principal(
			thingName = thingName,
			principal = certificateArn
	)
	

	


thingGroupName = "thing_group1"
thingClient = boto3.client('iot')
for i in range(50):
	thingName = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(15)])
	createThing(i)
	response = thingClient.add_thing_to_thing_group(thingGroupName=thingGroupName, thingName=thingName)

