import json
import logging
import sys
import greengrasssdk
from threading import Timer

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# Counter
my_counter = 0
def calc():
    global my_counter
    #TODO1: Get your data

    #TODO2: Calculate max CO2 emission
    vehicle0_vals = []
    with open("vehicle0.csv", "r") as file:
        count = 0
        for line in file:
            if count == 0:
                count += 1
                continue
            vals = line.strip().split(',')
            vehicle0_vals.append(vals[2])
    vehicle1_vals = []
    with open("vehicle1.csv", "r") as file:
        count = 0
        for line in file:
            if count == 0:
                count += 1
                continue
            vals = line.strip().split(',')
            vehicle1_vals.append(vals[2])
    vehicle2_vals = []
    with open("vehicle2.csv", "r") as file:
        count = 0
        for line in file:
            if count == 0:
                count += 1
                continue
            vals = line.strip().split(',')
            vehicle2_vals.append(vals[2])
    vehicle3_vals = []
    with open("vehicle3.csv", "r") as file:
        count = 0
        for line in file:
            if count == 0:
                count += 1
                continue
            vals = line.strip().split(',')
            vehicle3_vals.append(vals[2])
    vehicle4_vals = []
    with open("vehicle4.csv", "r") as file:
        count = 0
        for line in file:
            if count == 0:
                count += 1
                continue
            vals = line.strip().split(',')
            vehicle4_vals.append(vals[2])
    max_emission = [max(vehicle0_vals), max(vehicle1_vals), max(vehicle2_vals), max(vehicle3_vals), max(vehicle4_vals)]
    #TODO3: Return the result
    res = {"max_emission": max_emission}
    client.publish(
        topic="hello/world/counter",
        payload=json.dumps(
            {"message": "Hello world! Sent from Greengrass Core.  Invocation Count: {}".format(my_counter),
              "result": res}
        ),
    )
    my_counter += 1
    Timer(10, calc).start()

    return

calc()

def lambda_handler(event, context):
    return